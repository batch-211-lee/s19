//console.log("Hello World");

//What are conditional statements?

//Conditional Statements allow us to control the flow of our program
//It allows us to run a statement/instruction if the condition is met or run another seperate instruction if otherwise

//if,else if, else statement

let numA = -1;

//if statement
	//executes a statement if a certain/specified condition is true

	if(numA<0){
		console.log("Yes it is less than zero");
	};

	/*
		Syntax:
		if(condition){
			statement
		}
	*/

	//the result of the expression added in the if's condition must result to true, else, the statement inside if() will not run
	console.log(numA<0); //results to true -- the if statement will run

	numA = 0;
	if(numA<0){
		console.log("Hello");
	};

	console.log(numA<0); //false

	let city = "New York";

	if(city === "New York"){
		console.log("Welcome to New York City");
	};

	//else if clause

	/*
		-executes a statement if previous condition are false and if the specified condition is true
		-else if clause is optional and can be added to capture additional conditions to change the flow of a program
	*/

	let numH = 1

	if(numA<0){
		console.log("Hello");
	} else if (numH>0){
		console.log("World");
	};

	//we were able to run the else if() statement after we evaluated that the if condition failed
	//if the if() condition was passed and run, we will no longer evaluate the else if() and end the process

	numA = 1;

	if(numA>0){
		console.log("Hello");
	} else if (numH>0){
		console.log("World");
	};

	//else if() statement was no longer run because the if statement was able to run, the evaluation of the whole statement stops there

	city = "Tokyo";

	if(city === "New York"){
		console.log("Welcome to New York City!")
	} else if (city === "Tokyo"){
		console.log("Welcome to Tokyo, Japan");
	};

	//since we failed the condition for the first if(), we went to the else if() and checked and instead passed that condition

	//else statement

	/*
		-executes a statement if all other conditions are false
		-the "else" statement is optional and can be added to capture any other result to change the flow of a program
	*/

	if(numA<0){
		console.log("Hello");
	} else if(numH === 0){
		console.log("World");
	} else {
		console.log("Again");
	};

	/*
		Since both the preceeding if and else if conditions are not met/failed, the else statement was run instead

		Else statements should only be added if there is a preceeding if condition.
		Else statement by itself will not work, however, if statements will work even if there is no else statement
	*/

	//else{
	//	console.log("Will not run without an if");
	// }; //error

	// else if(numH === 0){
	// 	console.log("World");
	// } else{
	// 	console.log("Again");
	// }; //error

	//same goes for an else if, there should be a preceeding if() first

	/*
		-most of the time, we would like to use if, else if, and else statements with the functions to control the flow of our application
	*/

	let message = "No message.";
	console.log(message);

	function determineTyphoonIntensity(windSpeed){
		if (windSpeed < 30){
			return "Not a typhoon yet.";
		}else if (windSpeed <= 61){
			return "Tropical depression detected.";
		}else if(windSpeed >= 62 && windSpeed <= 88){
			return "Tropical Storm Detected";
		}else if(windSpeed >= 89 || windSpeed <= 117){
			return "Severe tropical storm detected";
		}else{
			return "Typhoon Detected";
		};
	};

	//returns the string to the variable "message" that invoked it
	message = determineTyphoonIntensity(110);
	console.log(message);

	if(message == "Severe tropical storm detected"){
		console.warn(message);
	};
	//console.warn() is a good way to print warnings in our console that could help us developers act on certain output with our code

	function oddOrEvenChecker(num1){
		if(num1%2 == 0){
			return "Even number";
		} else{
			return "Odd number"
		};
	};

	message = oddOrEvenChecker(4);
	if(message === "Even number"){
		console.warn(message);
	}else {
		console.log(message)
	};

	// function ageChecker(age){
	// 	if(age>=18){
	// 		alert(age + " Is old enough to drink");
	// 	}else{
	// 		alert(age + " Is not old enough to drink")
	// 	};
	// };

	// let isAllowedToDrink = ageChecker(18);
	

	//Truthy and Falsy

	/*
		- in JS a "truthy" is a value that is considered true when encountered in a Boolean context
		-Values in are considered true unless defined otherwise
		-Falsy values/exceptions for truth:
			1. False
			2. 0
			3. -0
			4. " "
			5. null
			6. undefined
			7. NaN
	*/

	//Truthy Examples
	/*
		-If a result of an expression in a condition results to a truthy value, the condition returns true and the corresponding statements are executed
		-Expressions are any unit of code that can be evaluated to a value
	*/

	if(" "){
		console.log("Truthy");
	}

	if(34){
		console.log("Truthy");
	}

	if([]){
		console.log("Truthy");
	}

	//Falsy Examples

	if(false){
		console.log("Falsy");
	}

	if(0){
		console.log("Falsy");
	}

	if(undefined){
		console.log("Falsy");
	}

	//Conditional (Ternary) Operator

	/*
		-The Conditional (Ternery) Operator takes three operands:
			1. condition
			2. expression to execute if the condition is truthy
			3. expression to execute if the condition is falsy

		-can be used as an alternative to an "if else" statement
		-ternary operators have an implicit "return" statement meaning, that without the "return" keyword, the result expressions can be stored in a variable

		syntax:
			(expression) ? ifTrue : ifFalse;
	*/

	//Single statement execution

	let ternaryResult = (1<18) ? true : false;
	console.log("Result of Ternary Operator: " + ternaryResult);

	//Multiple statement execution
	/*
		a function may be defined then used in a ternary operator
	*/

	let name;

	function isOfLegalAge(){
		name = "John";
		return "You are of legal age";
	};

	function isUnderAge(){
		name = "Jane";
		return "You are under the age limit";
	};

	let age = parseInt(prompt("What is your age?"));
	console.log(age);

	let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
	console.log("Result of Ternary Operator in Function: " + legalAge + ", " + name);

	//Switch Statement
	/*
		The switch statement avaluates an expression and matches the expression's value to a case clause

		.toLowerCase Function will change the input recieved from the prompt into all lowercase letters ensuring a match with the switch case
		-The "expression" is the information used to match the "value" provided in the switch cases
		-Variables are commonly used as expression to allow varying user input to be used from when comparing with switch case values
		-break statement is used to terminate the current loop once a match has been found
	*/

	let day = prompt("What day of the week is it today?").toLowerCase();
	console.log(day);

	switch (day) {
		case "monday" :
			console.log("the color of the day is red");
			break;
		case "tuesday" :
			console.log("the color of the day is orange");
			break;
		case "wednesday" :
			console.log("the color of the day is yellow");
			break;
		case "thursday" :
			console.log("the color of the day is green");
			break;
		case "friday" :
			console.log("the color of the day is blue");
			break;
		case "saturday" :
			console.log("the color of the day is indigo");
			break;
		case "sunday" :
			console.log("the color of the day is violet");
			break;
		default:
			console.log("Please input a valid day");
			break;
	};


	function determineBear(bearNumber){
		let bear;
		switch (bearNumber){
			case 1 :
				alert("Hi I'm Amy!");
				break;
			case 2 :
				alert("Hey, I'm Lulu");
				break;
			case 3 :
				alert("Hi, I'm Morgan");
				break;
			default:
				bear = bearNumber + "is out of bounds";
				break;
		}
		return bear;
	}
determineBear(2);

//Try-Catch-Finally Statement
/*
	-"try-ctach" statement are commonly used for error handling
	-they are used to specify a response whenever an error is received
*/

 function showIntensityAlert(windSpeed){
 	try{
 		alert(determineTyphoonIntensity(windSpeed))
 		//error/err are commonly used variable names used by developers for sorting erros
 	}catch(error){
 		console.log(typeof error)
 		//the error.message is used to access the information relating to an error object
 		console.warn(error.message);
 	}finally{
 		//continue execution of code regardless of success and failure of code execution in the "try" block to handle/resolve errors
 		alert("intensity updates will show ne alert");
 	};
 }

 showIntensityAlert(56);


